include .env

MIGRATIONS_PATH ?= file://migrations
POSTGRESQL_URL ?= postgresql://postgres:${POSTGRES_PASSWORD}@localhost:5432/postgres?sslmode=disable

PROTO_SRC = proto/souseka.proto
PROTO_GEN = ${PROTO_SRC:.proto=.pb.go} ${PROTO_SRC:.proto=_grpc.pb.go}

%.pb.go %_grpc.pb.go: %.proto
	protoc --go_out=. --go_opt=paths=source_relative \
		--go-grpc_out=. --go-grpc_opt=paths=source_relative $<

proto: ${PROTO_GEN}

run: ${PROTO_GEN}
	docker-compose up --build

migrate_up:
	migrate -source ${MIGRATIONS_PATH} \
		-database ${POSTGRESQL_URL} up 1

migrate_down:
	migrate -source ${MIGRATIONS_PATH} \
		-database ${POSTGRESQL_URL} down 1
psql:
	psql ${POSTGRESQL_URL}

grpcui:
	grpcui -plaintext localhost:${GRPC_PORT}

.PHONY: proto run migrate_up migrate_down psql
