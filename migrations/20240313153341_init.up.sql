BEGIN;

CREATE TABLE IF NOT EXISTS boards (
	id varchar NOT NULL,
	name varchar NOT NULL,
	description varchar NOT NULL,
	category varchar NOT NULL,
	bump_limit int NOT NULL,
	create_time timestamp NOT NULL,
	PRIMARY KEY(id)
);

/*
CREATE TABLE IF NOT EXISTS tags (
	id varchar NOT NULL,
	board_id varchar NOT NULL,
	PRIMARY KEY(id, board_id),
	FOREIGN KEY(board_id)
		REFERENCES boards(id)
		ON DELETE CASCADE
);
*/

CREATE TABLE IF NOT EXISTS threads (
	id int NOT NULL,
	board_id varchar NOT NULL,
	bump_time timestamp NOT NULL,
	PRIMARY KEY(id, board_id),
	FOREIGN KEY(board_id)
		REFERENCES boards(id)
		ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS posts (
	id int NOT NULL,
	board_id varchar NOT NULL,
	subject varchar,
	message varchar NOT NULL,
	create_time timestamp NOT NULL,
	thread_id int NOT NULL,
	PRIMARY KEY(id, board_id),
	FOREIGN KEY(thread_id, board_id)
		REFERENCES threads(id, board_id)
		ON DELETE CASCADE
);


/*
CREATE TABLE IF NOT EXISTS attachments (
	position int,
	post_id int,
	board_id varchar,
	filepath varchar NOT NULL,
	name varchar NOT NULL,
	width int,
	height int,
	duration int,
	PRIMARY KEY(position, post_id, board_id),
	FOREIGN KEY(post_id, board_id)
		REFERENCES posts(id, board_id)
		ON DELETE CASCADE,
);
*/

COMMIT;
