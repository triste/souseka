package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/caarlos0/env/v10"
	"gitlab.com/triste/souseka/internal/app"
)

func main() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	cfg := app.Config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("Failed to parse environment variables: %v", err)
	}
	app, err := app.NewApplication(cfg)
	if err != nil {
		log.Fatal("Failed to create application: %w", err)
	}
	go func() {
		if err := app.Start(); err != nil {
			log.Fatal("Application stopped: %w", err)
		}
	}()
	log.Printf("Signal `%v` received. Stopping...", <-sig)
	app.Stop()
}
