FROM golang:latest AS builder
WORKDIR /usr/src/souseka
COPY go.mod go.sum ./
RUN go mod download && go mod verify
COPY . .
RUN go build -v -o /usr/local/bin/ ./...
EXPOSE 50051
CMD [ "souseka" ]

#FROM alpine:latest
#WORKDIR /
#COPY --from=builder /usr/src/souseka/build/souseka /usr/local/bin/souseka1
#EXPOSE 50051
#ENTRYPOINT [ "/usr/local/bin/souseka1" ]
