package app

import (
	"gitlab.com/triste/souseka/internal/repo/postgres"
)

type Config struct {
	GrpcPort uint            `env:"GRPC_PORT" envDefault:"50051"`
	Postgres postgres.Config `envPrefix:"POSTGRES_"`
}
