package app

import (
	"errors"
	"fmt"
	"net"
	"sync/atomic"

	grpcCtrl "gitlab.com/triste/souseka/internal/controller/grpc"
	"gitlab.com/triste/souseka/internal/repo/postgres"
	"gitlab.com/triste/souseka/internal/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Application struct {
	config     Config
	postgres   *postgres.Postgres
	grpcServer *grpc.Server
	started    atomic.Bool
}

func NewApplication(cfg Config) (*Application, error) {
	pg, err := postgres.NewPostgres(cfg.Postgres)
	if err != nil {
		return nil, fmt.Errorf("postgres: %w", err)
	}
	boardRepo := postgres.NewBoardRepository(pg)
	thredRepo := postgres.NewThreadRepository(pg)
	postRepo := postgres.NewPostRepository(pg)
	txManager := postgres.NewTxManager(pg)
	service := service.NewService(boardRepo, thredRepo, postRepo, txManager)
	controller := grpcCtrl.NewController(service)
	server := grpc.NewServer()
	grpcCtrl.Register(server, controller)
	reflection.Register(server)
	return &Application{
		config:     cfg,
		postgres:   pg,
		grpcServer: server,
	}, nil
}

func (a *Application) Start() error {
	if !a.started.CompareAndSwap(false, true) {
		return errors.New("already started")
	}
	defer a.started.Store(false)
	address := fmt.Sprintf(":%v", a.config.GrpcPort)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("tcp listen %s: %w", address, err)
	}
	if err := a.grpcServer.Serve(listener); err != nil {
		return fmt.Errorf("grpc serve: %w", err)
	}
	return nil
}

func (a *Application) Stop() {
	a.grpcServer.GracefulStop()
	a.postgres.Close()
}
