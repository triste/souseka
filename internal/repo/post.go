package repo

import (
	"context"

	"gitlab.com/triste/souseka/internal/model"
)

type PostFilter struct {
	ThreadID    int
	StartFromID int
	AfterID     int
	Limit       int
}

type PostRepository interface {
	GetByFilter(ctx context.Context, boardID string, filter PostFilter, desc bool) []model.Post
	GetByID(ctx context.Context, boardID string, postID int) *model.Post
	Insert(ctx context.Context, post *model.Post) error
	Update(ctx context.Context, post *model.Post) error
	Delete(ctx context.Context, boardID string, postID int) error
}
