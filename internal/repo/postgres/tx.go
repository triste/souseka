package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/triste/souseka/internal/repo"
)

type queryable interface {
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...any) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}

type txKey struct{}

func getQueryable(ctx context.Context) queryable {
	if tx, ok := ctx.Value(txKey{}).(*sql.Tx); ok {
		return tx
	}
	return nil
}

func getQueryableNotNull(ctx context.Context, db *sql.DB) queryable {
	if tx := getQueryable(ctx); tx != nil {
		return tx
	}
	return db
}

func withTx(ctx context.Context, db *sql.DB, fn func(ctx context.Context, tx queryable) error) (e error) {
	tx := getQueryable(ctx)
	if tx == nil {
		newTx, err := db.BeginTx(ctx, nil)
		if err != nil {
			log.Panicf("begin transaction: %v", err)
		}
		defer func() {
			if e != nil {
				if err := newTx.Rollback(); err != nil {
					log.Panicf("rollback: %v", err)
				}
			} else {
				if err := newTx.Commit(); err != nil {
					log.Panicf("commit: %v", err)
				}
			}
		}()
		tx = newTx
	}
	return fn(ctx, tx)
}

type txManager struct {
	*Postgres
}

func contextWithTx(ctx context.Context, tx *sql.Tx) context.Context {
	return context.WithValue(ctx, txKey{}, tx)
}

func (t *txManager) WithTransaction(ctx context.Context, fn func(ctx context.Context) error) error {
	tx, err := t.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("begin transaction: %w", err)
	}
	ctx = contextWithTx(ctx, tx)
	if err := fn(ctx); err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func NewTxManager(postgres *Postgres) repo.TransactionManager {
	return &txManager{
		postgres,
	}
}
