package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/internal/repo"
)

type boardRepo struct {
	*Postgres
}

func (r *boardRepo) GetByFilter(ctx context.Context, filter repo.BoardFilter) []model.Board {
	query := `SELECT id, name, description, category, bump_limit, create_time FROM boards`
	var args []any
	if len(filter.StartFromID) > 0 {
		args = append(args, filter.StartFromID)
		query += fmt.Sprintf(" WHERE id >= $%v", len(args))
	}
	query += ` ORDER BY id`
	if filter.Limit > 0 {
		args = append(args, filter.Limit)
		query += fmt.Sprintf(" LIMIT $%v", len(args))
	}
	tx := getQueryableNotNull(ctx, r.db)
	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer rows.Close()
	boards := make([]model.Board, 0)
	for rows.Next() {
		board := model.Board{}
		err := rows.Scan(&board.ID, &board.Name, &board.Description,
			&board.Category, &board.BumpLimit, &board.CreateTime)
		if err != nil {
			log.Println(err)
			return nil
		}
		boards = append(boards, board)
	}
	return boards
}

func (r *boardRepo) GetByID(ctx context.Context, id string) *model.Board {
	const query = `
	SELECT name, description, category, bump_limit, create_time FROM boards
	WHERE id = $1
	`
	tx := getQueryableNotNull(ctx, r.db)
	row := tx.QueryRowContext(ctx, query, id)
	board := model.Board{
		ID: id,
	}
	err := row.Scan(&board.Name, &board.Description, &board.Category,
		&board.BumpLimit, &board.CreateTime)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println(err)
		}
		return nil
	}
	return &board
}

func (r *boardRepo) Insert(ctx context.Context, board *model.Board) error {
	const query = `
	INSERT INTO boards(id, name, description, category, bump_limit, create_time)
	VALUES ($1, $2, $3, $4, $5, $6)
	`
	return withTx(ctx, r.db, func(ctx context.Context, tx queryable) error {
		_, err := tx.ExecContext(ctx, query, board.ID, board.Name, board.Description,
			board.Category, board.BumpLimit, board.CreateTime)
		if err != nil {
			return err
		}
		query := fmt.Sprintf("CREATE SEQUENCE board_%v_post_num START 1 OWNED BY boards.id", board.ID)
		_, err = tx.ExecContext(ctx, query)
		return err
	})
}

func (r *boardRepo) Update(ctx context.Context, board *model.Board) error {
	const query = `
	UPDATE boards
	SET name = $2, description = $3, category = $4, bump_limit = $5, create_time = $6
	WHERE id = $1
	`
	tx := getQueryableNotNull(ctx, r.db)
	_, err := tx.ExecContext(ctx, query, board.ID, board.Name, board.Description,
		board.Category, board.BumpLimit, board.CreateTime)
	return err
}

func (r *boardRepo) Delete(ctx context.Context, id string) error {
	const query = `
	DELETE FROM boards
	WHERE id = $1
	`
	return withTx(ctx, r.db, func(ctx context.Context, tx queryable) error {
		res, err := tx.ExecContext(ctx, query, id)
		if err != nil {
			return err
		}
		deleteCount, err := res.RowsAffected()
		if err != nil {
			return err
		}
		if deleteCount == 0 {
			return errors.New("not found")
		}
		query := fmt.Sprintf("DROP SEQUENCE board_%v_post_num", id)
		_, err = tx.ExecContext(ctx, query)
		return err
	})
}
func (r *boardRepo) NextPostID(ctx context.Context, boardID string) int {
	query := fmt.Sprintf("SELECT nextval('board_%v_post_num')", boardID)
	tx := getQueryableNotNull(ctx, r.db)
	row := tx.QueryRowContext(ctx, query)
	var nextPostID int
	if err := row.Scan(&nextPostID); err != nil {
		log.Panicf("next post id: %v", err)
	}
	return nextPostID
}

func NewBoardRepository(postgres *Postgres) repo.BoardRepository {
	return &boardRepo{
		postgres,
	}
}
