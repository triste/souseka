package postgres

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/internal/repo"
)

type threadRepo struct {
	*Postgres
}

func (r *threadRepo) GetByIDAndUpdate(
	ctx context.Context,
	boardID string,
	threadID int,
	updateFn func(thread *model.Thread) error,
) error {
	const query = `
	SELECT bump_time FROM threads
	WHERE id = $1 AND board_id = $2
	FOR UPDATE
	`
	return withTx(ctx, r.db, func(ctx context.Context, tx queryable) error {
		row := tx.QueryRowContext(ctx, query, threadID, boardID)
		thread := model.Thread{
			ID:      threadID,
			BoardID: boardID,
		}
		if err := row.Scan(&thread.BumpTime); err != nil {
			if err != sql.ErrNoRows {
				log.Println(err)
			}
			return nil
		}
		if err := updateFn(&thread); err != nil {
			return err
		}
		const query = `
		UPDATE threads
		SET bump_time = $3
		WHERE id = $1 AND board_id = $2
		`
		_, err := tx.ExecContext(ctx, query, threadID, boardID, thread.BumpTime)
		return err
	})
}

func (r *threadRepo) Insert(ctx context.Context, thread *model.Thread) error {
	const query = `
	INSERT INTO threads(id, board_id, bump_time)
	VALUES ($1, $2, $3)
	`
	tx := getQueryableNotNull(ctx, r.db)
	_, err := tx.ExecContext(ctx, query, thread.ID, thread.BoardID, thread.BumpTime)
	return err
}

func NewThreadRepository(postgres *Postgres) repo.ThreadRepository {
	return &threadRepo{
		postgres,
	}
}
