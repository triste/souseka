package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type Config struct {
	User     string `env:"USER" envDefault:"postgres"`
	Password string `env:"PASSWORD,required"`
	Host     string `env:"HOST,required"`
	Port     uint   `env:"PORT" envDefault:"5432"`
	Db       string `env:"DB"`
}

type Postgres struct {
	db *sql.DB
}

func NewPostgres(cfg Config) (*Postgres, error) {
	if len(cfg.Db) == 0 {
		cfg.Db = cfg.User
	}
	connStr := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.Db)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	if err := db.Ping(); err != nil {
		return nil, err
	}
	return &Postgres{
		db,
	}, nil
}

func (p *Postgres) Close() {
	p.db.Close()
}
