package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/internal/repo"
)

type postRepo struct {
	*Postgres
}

type postDTO struct {
	id         int
	boardID    string
	subject    sql.NullString
	message    string
	createTime time.Time
	threadID   int
}

func newPostDTO(post *model.Post) postDTO {
	subject := sql.NullString{}
	if len(post.Subject) > 0 {
		subject.String = post.Subject
		subject.Valid = true
	}
	return postDTO{
		id:         post.ID,
		boardID:    post.BoardID,
		subject:    subject,
		message:    post.Message,
		createTime: post.CreateTime,
		threadID:   post.ThreadID,
	}
}

func (dto *postDTO) ToModel() model.Post {
	return model.Post{
		ID:         dto.id,
		BoardID:    dto.boardID,
		Subject:    dto.subject.String,
		Message:    dto.message,
		CreateTime: dto.createTime,
		ThreadID:   dto.threadID,
	}
}

func (r *postRepo) GetByFilter(
	ctx context.Context,
	boardID string,
	filter repo.PostFilter,
	desc bool,
) []model.Post {
	query := `
	SELECT id, subject, message, create_time, thread_id FROM posts
	WHERE board_id = $1
	`
	args := []any{
		boardID,
	}
	if filter.ThreadID > 0 {
		args = append(args, filter.ThreadID)
		query += fmt.Sprintf(" AND thread_id = $%v", len(args))
	}
	if filter.StartFromID > 0 {
		args = append(args, filter.StartFromID)
		query += fmt.Sprintf(" AND id >= $%v", len(args))
	}
	order := ""
	if desc {
		order = "DESC"
	} else {
		order = "ASC"
	}
	query += " ORDER BY id " + order
	if filter.Limit > 0 {
		args = append(args, filter.Limit)
		query += fmt.Sprintf(" LIMIT $%v", len(args))
	}
	tx := getQueryableNotNull(ctx, r.db)
	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer rows.Close()
	posts := make([]model.Post, 0)
	for rows.Next() {
		dto := postDTO{
			boardID: boardID,
		}
		err := rows.Scan(&dto.id, &dto.subject, &dto.message, &dto.createTime, &dto.threadID)
		if err != nil {
			log.Println(err)
			return nil
		}
		post := dto.ToModel()
		posts = append(posts, post)
	}
	return posts
}

func (r *postRepo) GetByID(ctx context.Context, boardID string, postID int) *model.Post {
	const query = `
	SELECT subject, message, create_time, thread_id FROM posts
	WHERE id = $1 AND board_id = $2
	`
	tx := getQueryableNotNull(ctx, r.db)
	row := tx.QueryRowContext(ctx, query, postID, boardID)
	dto := postDTO{
		id:      postID,
		boardID: boardID,
	}
	err := row.Scan(&dto.subject, &dto.message, &dto.createTime, &dto.threadID)
	if err != nil {
		return nil
	}
	post := dto.ToModel()
	return &post
}

func (r *postRepo) Insert(ctx context.Context, post *model.Post) (err error) {
	const query = `
	INSERT INTO posts(id, board_id, subject, message, create_time, thread_id)
	VALUES ($1, $2, $3, $4, $5, $6)
	`
	tx := getQueryableNotNull(ctx, r.db)
	dto := newPostDTO(post)
	_, err = tx.ExecContext(ctx, query, dto.id, dto.boardID, dto.subject,
		dto.message, dto.createTime, dto.threadID)
	return
}

func (r *postRepo) Update(ctx context.Context, post *model.Post) error {
	const query = `
	UPDATE posts
	SET subject = $3, message = $4, create_time = $5, thread_id = $6
	WHERE id = $1 AND board_id = $2
	`
	tx := getQueryableNotNull(ctx, r.db)
	dto := newPostDTO(post)
	_, err := tx.ExecContext(ctx, query, dto.id, dto.boardID, dto.subject,
		dto.message, dto.createTime, dto.threadID)
	return err
}

func (r *postRepo) Delete(ctx context.Context, boardID string, postID int) error {
	const query = `
	DELETE FROM posts
	WHERE id = $1 AND board_id = $2
	`
	tx := getQueryableNotNull(ctx, r.db)
	_, err := tx.ExecContext(ctx, query, postID, boardID)
	return err
}

func NewPostRepository(postgres *Postgres) repo.PostRepository {
	return &postRepo{
		postgres,
	}
}
