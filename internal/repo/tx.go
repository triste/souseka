package repo

import "context"

type TransactionManager interface {
	WithTransaction(context.Context, func(ctx context.Context) error) error
}
