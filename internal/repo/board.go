package repo

import (
	"context"

	"gitlab.com/triste/souseka/internal/model"
)

type BoardFilter struct {
	StartFromID string
	Limit       int
}

type BoardRepository interface {
	GetByFilter(ctx context.Context, filter BoardFilter) []model.Board
	GetByID(ctx context.Context, id string) *model.Board
	Insert(ctx context.Context, board *model.Board) error
	Update(ctx context.Context, board *model.Board) error
	Delete(ctx context.Context, name string) error
	NextPostID(ctx context.Context, boardID string) int
}
