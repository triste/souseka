package repo

import (
	"context"

	"gitlab.com/triste/souseka/internal/model"
)

type ThreadRepository interface {
	Insert(ctx context.Context, thread *model.Thread) error
	GetByIDAndUpdate(
		ctx context.Context,
		boardID string,
		threadID int,
		updateFn func(thread *model.Thread) error,
	) error
}
