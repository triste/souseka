package service

import (
	"context"
	"errors"
	"time"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/internal/repo"
)

// TODO: improve error handling
var ErrNotFound = errors.New("resource not found")
var ErrNotEmpty = errors.New("resource not empty")

type InvalidArgumentError struct {
	What string
}

func (e *InvalidArgumentError) Error() string {
	return e.What
}

func NewInvalidArgumentError(err error) error {
	return &InvalidArgumentError{
		What: err.Error(),
	}
}

type Service struct {
	boardRepo  repo.BoardRepository
	threadRepo repo.ThreadRepository
	postRepo   repo.PostRepository
	trm        repo.TransactionManager
}

func NewService(
	boardRepo repo.BoardRepository,
	threadRepo repo.ThreadRepository,
	postRepo repo.PostRepository,
	trm repo.TransactionManager,
) *Service {
	return &Service{
		boardRepo:  boardRepo,
		threadRepo: threadRepo,
		postRepo:   postRepo,
		trm:        trm,
	}
}

func (s *Service) GetBoard(ctx context.Context, id string) *model.Board {
	return s.boardRepo.GetByID(ctx, id)
}

func (s *Service) ListBoards(ctx context.Context, startFromID string, limit int) []model.Board {
	filter := repo.BoardFilter{
		StartFromID: startFromID,
		Limit:       limit,
	}
	return s.boardRepo.GetByFilter(ctx, filter)
}

func (s *Service) CreateBoard(
	ctx context.Context,
	id string,
	name string,
	description string,
	category string,
	bumpLimit int,
) (*model.Board, error) {
	board := &model.Board{
		ID:          id,
		Name:        name,
		Description: description,
		Category:    category,
		BumpLimit:   bumpLimit,
		CreateTime:  time.Now(),
	}
	if err := board.Validate(); err != nil {
		return nil, NewInvalidArgumentError(err)
	}
	if err := s.boardRepo.Insert(ctx, board); err != nil {
		return nil, err
	}
	return board, nil
}

func (s *Service) UpdateBoard(
	ctx context.Context,
	id string,
	name *string,
	description *string,
	category *string,
	bumpLimit *int,
) (*model.Board, error) {

	board := s.boardRepo.GetByID(ctx, id)
	if board == nil {
		return nil, ErrNotFound
	}
	if name != nil {
		board.Name = *name
	}
	if description != nil {
		board.Description = *description
	}
	if category != nil {
		board.Category = *category
	}
	if bumpLimit != nil {
		board.BumpLimit = *bumpLimit
	}
	if err := board.Validate(); err != nil {
		return nil, NewInvalidArgumentError(err)
	}
	if err := s.boardRepo.Update(ctx, board); err != nil {
		return nil, err
	}
	return board, nil
}

func (s *Service) DeleteBoard(ctx context.Context, id string, force bool) error {
	if !force {
		filter := repo.PostFilter{}
		posts := s.postRepo.GetByFilter(ctx, id, filter, false)
		if len(posts) != 0 {
			return ErrNotEmpty
		}
	}
	return s.boardRepo.Delete(ctx, id)
}

func (s *Service) GetPost(ctx context.Context, boardID string, postID int) *model.Post {
	return s.postRepo.GetByID(ctx, boardID, postID)
}

func (s *Service) ListPosts(
	ctx context.Context,
	boardID string,
	threadID int,
	startFromID int,
	afterID int,
	limit int,
	descending bool,
) ([]model.Post, error) {
	postFilter := repo.PostFilter{
		ThreadID:    threadID,
		StartFromID: startFromID,
		AfterID:     afterID,
		Limit:       limit,
	}
	return s.postRepo.GetByFilter(ctx, boardID, postFilter, descending), nil
}

func (s *Service) CreatePost(
	ctx context.Context,
	boardID string,
	subject string,
	message string,
	threadID int,
) (*model.Post, error) {
	board := s.boardRepo.GetByID(ctx, boardID)
	if board == nil {
		return nil, ErrNotFound
	}
	post := &model.Post{
		BoardID:    boardID,
		Subject:    subject,
		Message:    message,
		CreateTime: time.Now(),
	}
	if err := post.Validate(); err != nil {
		return nil, NewInvalidArgumentError(err)
	}
	err := s.trm.WithTransaction(ctx, func(ctx context.Context) error {
		if threadID != 0 {
			err := s.threadRepo.GetByIDAndUpdate(
				ctx,
				boardID,
				threadID,
				func(thread *model.Thread) error {
					filter := repo.PostFilter{
						ThreadID: threadID,
					}
					threadPosts := s.postRepo.GetByFilter(ctx, boardID, filter, false)
					if len(threadPosts) < board.BumpLimit {
						thread.BumpTime = post.CreateTime
					}
					return nil
				},
			)
			if err != nil {
				return err
			}
			// after checking the existence of the thread so as not to lose unique numbers
			post.ID = s.boardRepo.NextPostID(ctx, boardID)
		} else {
			post.ID = s.boardRepo.NextPostID(ctx, boardID)
			threadID = post.ID
			thread := model.Thread{
				ID:       threadID,
				BoardID:  boardID,
				BumpTime: post.CreateTime,
			}
			if err := s.threadRepo.Insert(ctx, &thread); err != nil {
				return err
			}
		}
		post.ThreadID = threadID
		if err := s.postRepo.Insert(ctx, post); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return post, nil
}

func (s *Service) UpdatePost(
	ctx context.Context,
	boardID string,
	postID int,
	subject *string,
	message *string,
) (*model.Post, error) {
	post := s.postRepo.GetByID(ctx, boardID, postID)
	if post == nil {
		return nil, ErrNotFound
	}
	if subject != nil {
		post.Subject = *subject
	}
	if message != nil {
		post.Message = *message
	}
	if err := post.Validate(); err != nil {
		return nil, err
	}
	if err := s.postRepo.Update(ctx, post); err != nil {
		return nil, err
	}
	return post, nil
}

func (s *Service) DeletePost(ctx context.Context, boardID string, postID int) error {
	return s.postRepo.Delete(ctx, boardID, postID)
}
