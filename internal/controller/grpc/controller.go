package grpc

import (
	"gitlab.com/triste/souseka/internal/service"
	"gitlab.com/triste/souseka/proto"
	"google.golang.org/grpc"
)

type Controller struct {
	proto.UnimplementedSousekaServer
	service *service.Service
}

func NewController(service *service.Service) proto.SousekaServer {
	return &Controller{
		service: service,
	}
}

func Register(server *grpc.Server, controller proto.SousekaServer) {
	proto.RegisterSousekaServer(server, controller)
}
