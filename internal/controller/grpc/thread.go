package grpc

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func parseThreadPath(path string) (string, int, error) {
	segments := strings.Split(path, "/")
	if len(segments) != 4 || segments[0] != "boards" || segments[1] != "threads" {
		return "", 0, errors.New("does not match the pattern")
	}
	threadID, err := strconv.Atoi(segments[2])
	if err != nil {
		return "", 0, fmt.Errorf("invalid thread id: %w", err)
	}
	boardID := segments[1]
	return boardID, threadID, nil
}

func buildThreadPath(boardID string, threadID int) string {
	segments := []string{"boards", boardID, "threads", strconv.Itoa(threadID)}
	return strings.Join(segments, "/")
}
