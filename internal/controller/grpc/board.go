package grpc

import (
	"context"
	"log"
	"strings"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func boardToProto(board *model.Board) *proto.Board {
	segments := []string{"boards", board.ID}
	name := strings.Join(segments, "/")
	return &proto.Board{
		Name:        name,
		DisplayName: board.Name,
		Description: board.Description,
		Category:    board.Category,
		BumpLimit:   int32(board.BumpLimit),
		CreateTime:  timestamppb.New(board.CreateTime),
	}
}

func parseBoardPath(path string) string {
	segments := strings.Split(path, "/")
	if len(segments) != 2 || segments[0] != "boards" {
		return ""
	}
	return segments[1]
}

type boardPageToken struct {
	StartFromID string `json:"start_from_id"`
}

func (c *Controller) GetBoard(
	ctx context.Context,
	req *proto.GetBoardRequest,
) (*proto.Board, error) {
	boardID := parseBoardPath(req.Name)
	if len(boardID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "name")
	}
	board := c.service.GetBoard(ctx, boardID)
	if board == nil {
		return nil, status.Error(codes.NotFound, "board not found")
	}
	return boardToProto(board), nil
}

func (c *Controller) ListBoards(
	ctx context.Context,
	req *proto.ListBoardsRequest,
) (*proto.ListBoardsResponse, error) {
	pagesize, err := newPageSize(req.PageSize, 50, 1000)
	if err != nil {
		return nil, err
	}
	startFromID := ""
	if len(req.PageToken) > 0 {
		token, err := pageTokenFromString[boardPageToken](req.PageToken)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "page token")
		}
		startFromID = token.StartFromID
	}
	boards := c.service.ListBoards(ctx, startFromID, pagesize+1)
	nextPageToken := ""
	if len(boards) > pagesize {
		nextBoard := boards[pagesize]
		boards = boards[:pagesize]
		token := boardPageToken{
			StartFromID: nextBoard.ID,
		}
		nextPageToken, err = pageTokenToString(token)
		if err != nil {
			log.Panicf("page token to string: %v", err)
		}
	}
	protoBoards := make([]*proto.Board, 0, len(boards))
	for _, board := range boards {
		protoBoard := boardToProto(&board)
		protoBoards = append(protoBoards, protoBoard)
	}
	return &proto.ListBoardsResponse{
		Boards:        protoBoards,
		NextPageToken: nextPageToken,
	}, nil
}

func (c *Controller) CreateBoard(
	ctx context.Context,
	req *proto.CreateBoardRequest,
) (*proto.Board, error) {
	if len(req.BoardId) == 0 {
		return nil, status.Error(codes.InvalidArgument, "board_id == nil")
	}
	if req.Board == nil {
		return nil, status.Error(codes.InvalidArgument, "board == nil")
	}
	board, err := c.service.CreateBoard(ctx, req.BoardId, req.Board.DisplayName,
		req.Board.Description, req.Board.Category, int(req.Board.BumpLimit))
	if err != nil {
		return nil, err
	}
	return boardToProto(board), nil
}

func (c *Controller) UpdateBoard(
	ctx context.Context,
	req *proto.UpdateBoardRequest,
) (*proto.Board, error) {
	if req.Board == nil {
		return nil, status.Error(codes.InvalidArgument, "board == nil")
	}
	boardID := parseBoardPath(req.Board.Name)
	if len(boardID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "board.name")
	}
	if req.UpdateMask == nil {
		return nil, status.Error(codes.InvalidArgument, "update_mask == nil")
	}
	var (
		name        *string
		description *string
		category    *string
		bumpLimit   *int
	)
	for _, path := range req.UpdateMask.Paths {
		switch path {
		case "display_name":
			name = &req.Board.DisplayName
		case "description":
			description = &req.Board.Description
		case "category":
			category = &req.Board.Category
		case "bump_limit":
			bl := int(req.Board.BumpLimit)
			bumpLimit = &bl
		}
	}
	board, err := c.service.UpdateBoard(ctx, boardID, name, description, category, bumpLimit)
	if err != nil {
		return nil, err
	}
	return boardToProto(board), nil
}

func (c *Controller) DeleteBoard(
	ctx context.Context,
	req *proto.DeleteBoardRequest,
) (*emptypb.Empty, error) {
	boardID := parseBoardPath(req.Name)
	if len(boardID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "name")
	}
	err := c.service.DeleteBoard(ctx, boardID, req.Force)
	return &emptypb.Empty{}, err
}
