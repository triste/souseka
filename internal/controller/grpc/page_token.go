package grpc

import (
	"encoding/base64"
	"encoding/json"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var errInvalidPageToken = status.Error(codes.InvalidArgument, "page token")

func pageTokenFromString[T any](str string) (*T, error) {
	decoded, err := base64.RawStdEncoding.DecodeString(str)
	if err != nil {
		return nil, errInvalidPageToken
	}
	var token T
	if err := json.Unmarshal(decoded, &token); err != nil {
		return nil, errInvalidPageToken
	}
	return &token, nil
}

func pageTokenToString(token any) (string, error) {
	data, err := json.Marshal(token)
	if err != nil {
		return "", err
	}
	encoded := base64.RawURLEncoding.EncodeToString(data)
	return encoded, nil
}
