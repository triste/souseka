package grpc

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func newPageSize(reqPageSize int32, defval, maxval int) (int, error) {
	if reqPageSize < 0 {
		return 0, status.Error(codes.InvalidArgument, "page size < 0")
	}
	if reqPageSize == 0 {
		return defval, nil
	}
	return min(int(reqPageSize), maxval), nil
}
