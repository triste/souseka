package grpc

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/triste/souseka/internal/model"
	"gitlab.com/triste/souseka/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func parsePostPath(path string) (boardID string, postID int, ok bool) {
	segments := strings.Split(path, "/")
	if len(segments) != 4 || segments[0] != "boards" || segments[2] != "posts" {
		return "", 0, false
	}
	postID, err := strconv.Atoi(segments[3])
	if err != nil {
		return "", 0, false
	}
	return segments[1], postID, true
}

func postToProto(post *model.Post) *proto.Post {
	postID := strconv.Itoa(post.ID)
	segments := []string{"boards", post.BoardID, "posts", postID}
	name := strings.Join(segments, "/")
	thread := buildThreadPath(post.BoardID, post.ThreadID)
	return &proto.Post{
		Name:       name,
		Subject:    post.Subject,
		Text:       post.Message,
		CreateTime: timestamppb.New(post.CreateTime),
		Thread:     thread,
	}
}

type postPageToken struct {
	StartFromID int `json:"start_from_id,omitempty"`
}

func (c *Controller) GetPost(ctx context.Context, req *proto.GetPostRequest) (*proto.Post, error) {
	boardID, postID, ok := parsePostPath(req.Name)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, "name")
	}
	post := c.service.GetPost(ctx, boardID, postID)
	if post == nil {
		return nil, status.Error(codes.NotFound, "post not found")
	}
	return postToProto(post), nil
}

func (c *Controller) ListPosts(ctx context.Context, req *proto.ListPostsRequest) (*proto.ListPostsResponse, error) {
	boardID := parseBoardPath(req.Parent)
	threadID := 0
	if len(boardID) == 0 {
		bid, tid, err := parseThreadPath(req.Parent)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "parent")
		}
		boardID = bid
		threadID = tid
	}
	pagesize, err := newPageSize(req.PageSize, 100, 1000)
	if err != nil {
		return nil, err
	}
	startFromID := 0
	if len(req.PageToken) > 0 {
		token, err := pageTokenFromString[postPageToken](req.PageToken)
		if err != nil {
			return nil, err
		}
		startFromID = token.StartFromID
	}
	desc := false
	if len(req.OrderBy) > 0 {
		switch req.OrderBy {
		case "post_id ASC":
			desc = false
		case "post_id DESC":
			desc = true
		}
	}
	filterAfterID := 0
	if len(req.Filter) > 0 {
		if _, err := fmt.Scanf("post_id > %v", filterAfterID); err != nil {
			return nil, status.Error(codes.InvalidArgument, "filter")
		}
	}
	posts, err := c.service.ListPosts(ctx, boardID, threadID, startFromID, filterAfterID,
		pagesize+1, desc)
	if err != nil {
		return nil, err
	}
	nextPageToken := ""
	if len(posts) > pagesize {
		nextPost := posts[pagesize]
		posts = posts[:pagesize]
		token := postPageToken{
			StartFromID: nextPost.ID,
		}
		nextPageToken, err = pageTokenToString(token)
		if err != nil {
			log.Panicf("page token to string: %v", err)
		}
	}
	protoPosts := make([]*proto.Post, 0, len(posts))
	for _, post := range posts {
		protoPost := postToProto(&post)
		protoPosts = append(protoPosts, protoPost)
	}
	return &proto.ListPostsResponse{
		Posts:         protoPosts,
		NextPageToken: nextPageToken,
	}, nil
}

func (c *Controller) CreatePost(
	ctx context.Context,
	req *proto.CreatePostRequest,
) (*proto.Post, error) {
	boardID := parseBoardPath(req.Parent)
	if len(boardID) == 0 {
		return nil, status.Error(codes.InvalidArgument, "parent")
	}
	if req.Post == nil {
		return nil, status.Error(codes.InvalidArgument, "post == nil")
	}
	threadID := 0
	if len(req.Post.ThreadId) > 0 {
		var err error
		threadID, err = strconv.Atoi(req.Post.ThreadId)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "thread_id")
		}
	}
	post, err := c.service.CreatePost(ctx, boardID, req.Post.Subject, req.Post.Text, threadID)
	if err != nil {
		return nil, err
	}
	return postToProto(post), nil
}
