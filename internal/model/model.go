package model

import (
	"errors"
	"time"
)

type Board struct {
	ID          string
	Name        string
	Description string
	Category    string
	BumpLimit   int
	CreateTime  time.Time
}

func (b *Board) Validate() error {
	if len(b.ID) == 0 {
		return errors.New("id lenght == 0")
	}
	if len(b.Name) == 0 {
		return errors.New("name lenght == 0")
	}
	if len(b.Description) == 0 {
		return errors.New("description lenght == 0")
	}
	if len(b.Category) == 0 {
		return errors.New("category lenght == 0")
	}
	if b.BumpLimit <= 0 {
		return errors.New("bump limit <= 0")
	}
	if b.CreateTime.IsZero() {
		return errors.New("create time == 0")
	}
	return nil
}

type Post struct {
	ID         int
	BoardID    string
	Subject    string
	Message    string
	CreateTime time.Time
	ThreadID   int
}

func (p *Post) Validate() error {
	if p.ID < 0 {
		return errors.New("id < 0")
	}
	if len(p.Message) == 0 {
		return errors.New("message lenght == 0")
	}
	if p.CreateTime.IsZero() {
		return errors.New("create time == 0")
	}
	if p.ThreadID < 0 {
		return errors.New("thread id < 0")
	}
	return nil
}

type Thread struct {
	ID       int
	BoardID  string
	BumpTime time.Time
}
